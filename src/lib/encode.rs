extern crate image;

use std::cmp;

pub fn to_img(data: &[u8], bit_density: u8) -> image::RgbImage {
    if bit_density < 1 || bit_density > 8 {
        panic!("Wrong bit density")
    }

    let mut data_iter = data.into_iter().peekable();
    let mut remaining_bits = 0 as usize;
    let mut curr_dat = 0 as u32;

    let (mut data_size, h, w) = img_size(data.len(), bit_density);
    let mut img: image::RgbImage = image::ImageBuffer::new(w, h);

    let bit_dens_val = ((bit_density as (u16) << 5) - 1) as u8;
    *img.get_pixel_mut(0, 0) = image::Rgb([bit_dens_val, bit_dens_val, bit_dens_val]);

    *img.get_pixel_mut(1%w, 1/w) = image::Rgb([stsp(&mut data_size), stsp(&mut data_size), stsp(&mut data_size)]);
    *img.get_pixel_mut(2%w, 2/w) = image::Rgb([stsp(&mut data_size), stsp(&mut data_size), stsp(&mut data_size)]);

    println!("{:?} {}x{}", img.get_pixel(1%w, 1/w), h, w);

    for y in 0..h {
        for x in 0..w {
            if y*w+x < 3 {
                continue;
            }

            while remaining_bits < bit_density as usize*3 && data_iter.peek().is_some() {
                curr_dat += u32::from(*data_iter.next().unwrap()) << remaining_bits;
                remaining_bits += 8;
            }

            let red = extract_val(&mut curr_dat, &mut remaining_bits, bit_density);
            let blue = extract_val(&mut curr_dat, &mut remaining_bits, bit_density);
            let green = extract_val(&mut curr_dat, &mut remaining_bits, bit_density);

            let pix = img.get_pixel_mut(x, y);
            *pix = image::Rgb([red, green, blue]);
        }
    }

    img
}

fn extract_val(curr_dat: &mut u32, remain: &mut usize, bit_density: u8) -> u8 {
    let mut ret = *curr_dat % (1 << bit_density);
    ret <<= 8 - bit_density;
    let ret8 = ret as u8;
    *curr_dat >>= bit_density;
    *remain -= cmp::min(*remain, bit_density as usize);

    ret8
}

fn img_size(size: usize, bit_density: u8) -> (u64, u32, u32) {
    let data_size = (size as f32 * 8.0 / (bit_density as f32 * 3.0)).ceil() as u64;
    let pixels = data_size + 3;
    let h = (pixels as f64).sqrt().floor() as u32;
    let w = (pixels as f32 / h as f32).ceil() as u32;

    (data_size, h, w)
}

fn stsp(data_size: &mut u64) -> u8 {
    match *data_size {
        0 => 0,
        _ => {
            let ret: u8 = (*data_size % (1 << 8)) as u8;
            *data_size >>= 8;

            ret
        }
    } 
}