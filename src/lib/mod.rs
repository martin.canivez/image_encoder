extern crate image;

mod encode;
mod decode;

//pub use decode::from_img;
pub use encode::to_img;