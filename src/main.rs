mod lib;

fn main() {
    let rand_bytes: Vec<u8> = (0..1024).map(|_| {rand::random::<u8>()}).collect();

    for i in 1..9 {
        let img = lib::to_img(&rand_bytes, i);
        img.save(format!("out/out{}.png", i)).unwrap();
    }
}